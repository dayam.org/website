# Dayaṁ Media

https://media.dayam.org/

## Usage

1. Install the dependencies:

   ```sh
   ./setup.sh
   ```

2. Configure `s3cmd` to access the `media.dayam.org` bucket.

   ```sh
   cp s3cmd-config.example s3cmd-config
   chmod 600 s3cmd-config
   vim s3cmd-config
   ```

5. Deploy

   ```sh
   ./deploy
   ```
