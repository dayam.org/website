from hashlib import md5
import json
from pathlib import Path
from PIL import Image
import re
from urllib.request import urlopen


MASTODON_DOMAIN = "mastodon.online"
MASTODON_USER = "jagaro"
FIRST_STATUS_ID = 110211502052336337
LAST_STATUS_ID = 110528542034611134

RESPONSIVE_SIZES = [640, 960, 1280, 1600]

BASE_DIR = Path(__file__).parent
JSON_CACHE_DIR = BASE_DIR / "json_cache"
PROCESSED_POSTS_PATH = BASE_DIR / "posts.json"

PUBLIC_DIR = BASE_DIR.parent / "public"
IMAGES_DIR = PUBLIC_DIR / "tudong-2023"
ORIGINAL_IMAGES_DIR = IMAGES_DIR / "original"
RESIZED_IMAGES_DIR = IMAGES_DIR / "resized"
IMAGE_MANIFEST_PATH = BASE_DIR / "image_manifest.json"
IMAGE_BASE_URL = "https://media.dayam.org/tudong-2023"


def fetch(url, path):
    if not path.exists():
        print(f"Fetching {url}")
        with urlopen(url) as f:
            path.parent.mkdir(exist_ok=True, parents=True)
            path.open("wb").write(f.read())


def fetch_json(url):
    filename = md5(str(url).encode()).hexdigest() + ".json"
    path = JSON_CACHE_DIR / filename
    fetch(url, path)
    return json.load(path.open())


def fetch_image(url, base):
    ext = re.sub(r"^.*\.([A-Za-z0-9]+)$", "\\1", url)
    filename = f"{base}.{ext}"
    path = ORIGINAL_IMAGES_DIR / filename
    fetch(url, path)
    return path


def write_json(data, path):
    path.parent.mkdir(exist_ok=True, parents=True)
    json.dump(
        data,
        path.open("w"),
        indent=4,
        sort_keys=True,
    )


def resize_image(path):
    with Image.open(path) as image:
        width, height = image.size
        info = {
            "original": {
                "width": width,
                "height": height,
                "url": f"{IMAGE_BASE_URL}/original/{path.name}",
            },
            "resized": [],
        }
        for i, rwidth in enumerate(RESPONSIVE_SIZES):
            if i != 0 and width < rwidth and width < RESPONSIVE_SIZES[i - 1]:
                continue
            rheight = int((height * rwidth) / width)
            resized = image.copy()
            resized = image.resize((rwidth, rheight))
            resized_path = RESIZED_IMAGES_DIR / f"{path.stem}-{rwidth}"
            resized_path_jpeg = resized_path.with_suffix(".jpg")
            resized_path_webp = resized_path.with_suffix(".webp")
            if not resized_path_jpeg.exists():
                print(f"  -> {resized_path_jpeg.relative_to(IMAGES_DIR)}")
                resized.save(resized_path_jpeg, "JPEG", optimize=True, quality=75)
            if not resized_path_webp.exists():
                print(f"  -> {resized_path_webp.relative_to(IMAGES_DIR)}")
                resized.save(resized_path_webp, "WEBP", method=6, quality=80)
            info["resized"].append(
                {
                    "width": rwidth,
                    "height": rheight,
                    "url_jpeg": f"{IMAGE_BASE_URL}/resized/{resized_path_jpeg.name}",
                    "url_webp": f"{IMAGE_BASE_URL}/resized/{resized_path_webp.name}",
                }
            )
        return info


url = f"https://{MASTODON_DOMAIN}/api/v1/accounts/lookup?acct={MASTODON_USER}"
account_id = fetch_json(url)["id"]

unprocessed_posts = []

status_id = FIRST_STATUS_ID
while status_id <= LAST_STATUS_ID:
    url = (
        f"https://{MASTODON_DOMAIN}/api/v1/accounts/{account_id}/statuses?"
        + f"min_id={status_id}&max_id={LAST_STATUS_ID + 1}"
    )
    posts = fetch_json(url)
    if not len(posts):
        break
    posts.sort(key=lambda post: int(post["id"]))
    for post in posts:
        unprocessed_posts.append(post)
        status_id = int(post["id"])

processed_posts = []

for post in unprocessed_posts:
    id = int(post["id"])
    print(f"Processing {id}")

    content = post["content"]
    images = []
    card = None

    content = re.sub(
        r'<p><a href="https://(youtu.be|urbanists\.video)/.+</p>', "", content
    )

    for i, attachment in enumerate(post["media_attachments"] or []):
        url = attachment["url"]
        meta = attachment.get("meta", {})

        image_path = fetch_image(url, f"{id}-image-{i + 1}")
        image_info = resize_image(image_path)
        image_info["alt"] = attachment.get("description")
        image_info["focus"] = meta.get("focus", {"x": 0, "y": 0})
        images.append(image_info)

    if post["card"]:
        video_provider = post["card"]["provider_url"]
        if video_provider:
            video_url = post["card"]["url"]

            if video_provider == "https://urbanists.video":
                provider = "peertube"
                video_id = re.sub(r"^.+/([^/]+)$", "\\1", video_url)
                video_api_url = f"{video_provider}/api/v1/videos/{video_id}"
                video_api_data = fetch_json(video_api_url)
                url = f"{video_provider}{video_api_data['previewPath']}"
            elif video_provider == "https://www.youtube.com/":
                provider = "youtube"
                video_id = re.sub(r"^.+\?v=(.+)&.+$", "\\1", video_url)
                video_url = f"https://youtu.be/{video_id}"
                url = f"https://i.ytimg.com/vi/{video_id}/maxresdefault.jpg"
            else:
                raise Exception(f"Unknown provider {video_provider}")

            image_path = fetch_image(url, f"{id}-card")
            image_info = resize_image(image_path)
            image_info["alt"] = post["card"]["title"]
            image_info["focus"] = {"x": 0, "y": 0}
            card = {
                "provider": provider,
                "url": video_url,
                "video_id": video_id,
                "image": image_info,
            }

    processed_posts.append(
        {
            "card": card,
            "content": content,
            "id": str(id),
            "images": images,
            "published": post["created_at"],
            "url": post["url"],
        }
    )

write_json(processed_posts, PROCESSED_POSTS_PATH)
