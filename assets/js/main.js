// https://stackoverflow.com/a/6234804
const escapeHtml = (unsafe) => {
  return String(unsafe)
    .replaceAll("&", "&amp;")
    .replaceAll("<", "&lt;")
    .replaceAll(">", "&gt;")
    .replaceAll('"', "&quot;")
    .replaceAll("'", "&#039;");
};

const showImage = (link) => {
  basicLightbox.create('<img src="' + escapeHtml(link.href) + '" />').show();
  return false;
};

// There doesn't seem to be an easy way to play a video with sound.
const showPeerTubeVideo = (link, videoId) => {
  link.outerHTML =
    '<iframe src="https://urbanists.video/videos/embed/' +
    videoId +
    '?autoplay=1&muted=1" allowfullscreen frameborder="0"></iframe>';
  return false;
};

let _youTubeScriptTag = null;
const _youTubeDeferredPlays = [];

function loadYouTubeAPI() {
  if (!_youTubeScriptTag) {
    console.log('Loading YouTube Player');
    _youTubeScriptTag = document.createElement('script');
    _youTubeScriptTag.src = "https://www.youtube.com/iframe_api";
    const firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(_youTubeScriptTag, firstScriptTag);
  }
}

function onYouTubeIframeAPIReady() {
  while (_youTubeDeferredPlays.length) {
    const d = _youTubeDeferredPlays.pop();
    new window.YT.Player(d[0], {
      width: '100%',
      playerVars: {
        autoplay: 1,
        modestbranding: 1,
      },
      videoId: d[1],
    });
  }
}

function showYouTubeVideo(link, videoId) {
  if (!link.getAttribute("video-loading")) {
    link.setAttribute("video-loading", "1");
    loadYouTubeAPI();
    _youTubeDeferredPlays.push([link, videoId]);
  }
  return false;
}
