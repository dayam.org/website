# GPS Logger Notes

_This is not used anymore._

Notes on a GPS logging method to track movements in a non-granular way.

## SFTP Setup

On website host as `root`:

```sh
getent group gpslogger >/dev/null || groupadd gpslogger
getent passwd gpslogger >/dev/null ||
  useradd -g gpslogger -s /bin/bash -m gpslogger
sudo -u gpslogger -i bash <<'EOF'
  cd "$HOME"
  mkdir -p upload .ssh
  ssh-keygen -t ed25519 -f .ssh/id_ed25519
  # Enter a passphrase
  cp .ssh/id_ed25519.pub .ssh/authorized_keys
  chmod 700 .ssh
  chmod 600 .ssh/authorized_keys
EOF
```

Add to `/etc/ssh/sshd_config`:

```
Match User gpslogger
    ChrootDirectory /var/sftp
    ForceCommand internal-sftp
    PermitTunnel no
    AllowAgentForwarding no
    AllowTcpForwarding no
    X11Forwarding no
```

```sh
systemctl restart sshd
mkdir -p /var/sftp/gpslogger/raw
chown -R gpslogger:gpslogger /var/sftp/gpslogger
```

### GPX Setup

```sh
apt-get update
apt-get install -y gpsbabel

cat >/etc/cron.hourly/gpslogger <<'XEOF'
#!/bin/bash

sudo -u gpslogger -i <<'EOF'
  cd /var/sftp/gpslogger/raw
  inargs=()
  for g in *.gpx; do
    inargs+=("-i" "gpx" "-f" "$g")
  done
  gpsbabel "${inargs[@]}" \
    -x track,trk2seg -x simplify,error=0.5 \
    -o gpx -F /var/sftp/gpslogger/map.gpx
  perl -0777 -pi -e \
    's/ *<\/trkseg>\n *<trkseg>\n//gm' \
    /var/sftp/gpslogger/map.gpx
EOF

cp -f /var/sftp/gpslogger/map.gpx /opt/dayam/website/public/map.gpx
XEOF
chmod 755 /etc/cron.hourly/gpslogger
```
