# `dayam.org` Website

This repository is for https://dayam.org/.

## Setup

Install [Git](https://git-scm.com/), [Hugo](https://gohugo.io/) and
[NVM](https://github.com/nvm-sh/nvm).

```sh
git clone https://gitlab.com/dayam.org/website.git
cd website
git submodule update --init --recursive
nvm use
npm install
npm run login
npm run create-project # only needed first time
```

## Develop

```sh
npm run dev
```

Then go to http://localhost:1313/.

## Deploy

```sh
npm run deploy
```

## Update `hugo-scroll` submodule

```sh
git submodule update --recursive --remote
```

## Credits

- [Hugo](https://gohugo.io/)
- https://github.com/zjedi/hugo-scroll
- https://github.com/electerious/basicLightbox
- https://gitlab.com/idotj/mastodon-embed-feed-timeline
