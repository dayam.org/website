---
title: "About"
weight: 10
header_menu: true
---

As of mid-April, 2023, I am on a traditional monastic pilgrimage to walk along
the coastal areas of California.

In keeping with my monastic rules, I will only accept food that is offered
that day, not use money, and sleep wherever there is availibility.

This monastic pilgrimage often called "tudong." This is a Thai derived word
for the Pāli word "dhutaṅga," which means "austere practice."
