---
title: "Journal"
weight: 20
header_menu: true
---

{{< mastodon 3 >}}

{{< rawhtml >}}
<p><a href="https://mastodon.online/@jagaro" target="_blank">Read older posts on Mastodon...</a></p>
{{< /rawhtml >}}
