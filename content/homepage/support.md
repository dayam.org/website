---
title: "Support"
weight: 80
---

{{< image src="/images/jagaro-pack.jpg" pos="50% 0%" alt="Jāgaro's Pack" >}}

With a few exceptions, I have no planned support for this journey. I will be
relying on the internal qualities of patience and determination, as well as the
kindness and generosity of others.
