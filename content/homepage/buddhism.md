---
title: "Buddhism"
weight: 70
---
{{< rawhtml >}}

<p>People sometime ask, "what is Buddhism?"</p>

<p>Often what comes to my mind is a beautiful teaching
by <a href="https://en.wikipedia.org/wiki/Ajahn_Chah"
target="_blank">Ajahn Chah</a> offered to one of his
dying disciples.</p>

<audio controls>
  <source src="https://media.dayam.org/audio/our-real-home.ogg" type="audio/ogg" />
  <source src="https://media.dayam.org/audio/our-real-home.mp3" type="audio/mpeg" />
</audio>

<p>The above was read by Ajahn Pasanno, one of Ajahn
Chah's western disciples. It was <a
href="https://www.abhayagiri.org/talks/7918-ajahn-chah-our-real-home"
target="_blank">recorded in 2022</a> at Abhayagiri. If
you prefer, you can <a
href="https://www.abhayagiri.org/media/books/Chah_The_Collected_Teachings_of_Ajahn_Chah.pdf#page=183"
target="_blank">read it here</a>.</p>

{{< /rawhtml >}}
