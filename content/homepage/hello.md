---
title: "Hello"
weight: 1
---

My name is Tan Jāgaro, and I am a Buddhist Monk.

{{< rawhtml >}}
<audio controls autoplay loop>
  <source src="https://media.dayam.org/audio/ocean.ogg" type="audio/ogg" />
  <source src="https://media.dayam.org/audio/ocean.mp3" type="audio/mpeg" />
</audio>
{{< /rawhtml >}}
