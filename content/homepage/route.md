---
title: "Route"
weight: 40
---

The pilgrimage will start in the Ventura county area, northwest of Los Angeles.
The goal is to reach Santa Cruz, which is just south of San Francisco. I will
make my way through Santa Barbara, then north through Santa Ynez, then to
Orcutt, then finally to San Luis Obispo.

After San Luis Obispo, I will be joined by Ryan
and go to Santa Margarita, then Atascadero, then
Templeton, then Cambria and then Ragged Point.
Ryan will leave the pilgrimage after going to
Ragged Point.

_Update:_ I originally planned to head up Highway
1 along the coast but that
[that turned out to be too difficult](
https://mastodon.online/@jagaro/110464564991459061)

After that, I will leapfrog to Carmel Valley with
the assistance of a car ride. Finally, I will
complete the journey by going to Monterey and
then Santa Cruz.
