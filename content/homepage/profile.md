---
title: "Tan Jāgaro"
header_menu_title: "Profile"
navigation_menu_title: "Profile"
weight: 50
header_menu: true
---

{{< image src="/images/jagaro-standing.jpg" pos="50% 10%" alt="Tan Jāgaro" >}}

I began Buddhist practice in 2004 as a lay person after attending a meditation
retreat. Inspired by the Pāḷi Canon and more contemporary masters, my confidence
in the Buddha's teachings increased over the years.

{{< rawhtml >}}

<p>
  I first visited
  <a href="https://www.abhayagiri.org/" target="_blank">Abhayagiri Monastery</a>
  in 2012 and later practiced under
  <a href="https://en.wikipedia.org/wiki/Ajahn_Pasanno" target="_blank"
    >Ajahn Pasanno</a
  >
  and his community. In 2015 I went forth as a Sāmaṇera, and then in 2016 I took
  full Bhikkhu precepts. I left Ahayagiri in 2020 and have been traveling ever
  since. More recently, I have visited
  <a href="https://suddhavari.org/" target="_blank">Suddhavāri Monastery</a> and
  <a href="https://buddhistinsights.org/" target="_blank"
    >Empty Cloud Monastery</a
  >.
</p>

{{< /rawhtml >}}
